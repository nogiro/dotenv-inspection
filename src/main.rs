fn main() {
    match std::env::args().nth(1) {
        Some(x) if x == "dotenv" => dotenv(),
        _ => envmnt(),
    }
}

fn envmnt() {
    envmnt::load_file(".env").unwrap();
    print();
}

fn dotenv() {
    dotenv::dotenv().expect("failed to load environments by dotenv.");
    print();
}

fn print() {
    let _: Vec<()> = std::env::vars()
        .into_iter()
        .filter(|(key, _val)| key.starts_with("DOTENV_INSPECTION"))
        .map(|(key, val)| println!("{}={}", key, val))
        .collect();
}
