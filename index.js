const dotenv = require("dotenv");

dotenv.config();

Object.keys(process.env)
  .filter((key) => key.startsWith("DOTENV_INSPECTION"))
  .map((key) => console.info("%s=%s", key, process.env[key]));
