# dotenv-inspection

This repository summarizes the results of a inspection of [dotenv](https://www.npmjs.com/package/dotenv)-like libraries.

# OS

```sh
uname -a
# => Linux exp 5.10.0-19-amd64 #1 SMP Debian 5.10.149-2 (2022-10-21) x86_64 GNU/Linux

cat /etc/os-release
# => PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
# => NAME="Debian GNU/Linux"
# => VERSION_ID="11"
# => VERSION="11 (bullseye)"
# => VERSION_CODENAME=bullseye
# => ID=debian
# => HOME_URL="https://www.debian.org/"
# => SUPPORT_URL="https://www.debian.org/support"
# => BUG_REPORT_URL="https://bugs.debian.org/"
```

# dotenv (npm)

https://www.npmjs.com/package/dotenv

```sh
node --version
# => v18.13.0
npm --version
# => 9.3.1

npm run --silent env
# => DOTENV_INSPECTION_NOT_QUOTED=foo
# => DOTENV_INSPECTION_SINGLE_QUOTED=foo#bar
# => DOTENV_INSPECTION_DOUBLE_QUOTED=foo#bar
```

# Rust

+ envmnt: https://crates.io/crates/envmnt
  - `envmnt` is depended by `cargo-make` (https://crates.io/crates/cargo-make).
+ dotenv: https://crates.io/crates/dotenv

```sh
rustc --version
# => rustc 1.66.1 (90743e729 2023-01-10)
cargo make --version
# => cargo-make 0.36.3

cargo run --release --quiet envmnt
# => DOTENV_INSPECTION_NOT_QUOTED=foo#bar
# => DOTENV_INSPECTION_SINGLE_QUOTED='foo#bar'
# => DOTENV_INSPECTION_DOUBLE_QUOTED=foo#bar

cargo make --quiet --env-file .env env
# => DOTENV_INSPECTION_SINGLE_QUOTED='foo#bar'
# => DOTENV_INSPECTION_NOT_QUOTED=foo#bar
# => DOTENV_INSPECTION_DOUBLE_QUOTED=foo#bar

cargo run --release --quiet dotenv
# => DOTENV_INSPECTION_NOT_QUOTED=foo#bar
# => DOTENV_INSPECTION_SINGLE_QUOTED=foo#bar
# => DOTENV_INSPECTION_DOUBLE_QUOTED=foo#bar
```

# Python

```sh
poetry run -- python3 --version
# => Python 3.9.2
poetry --version
# => Poetry (version 1.3.2)

poetry run python3 main.py
# => DOTENV_INSPECTION_NOT_QUOTED=foo#bar
# => DOTENV_INSPECTION_SINGLE_QUOTED=foo#bar
# => DOTENV_INSPECTION_DOUBLE_QUOTED=foo#bar
```
