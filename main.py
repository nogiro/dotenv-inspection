import os
from dotenv import load_dotenv

load_dotenv()

for key, val in os.environ.items():
    if key.startswith("DOTENV_INSPECTION"):
        print("{}={}".format(key, val))
